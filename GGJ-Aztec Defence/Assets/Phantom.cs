﻿using UnityEngine;
using System.Collections;

public class Phantom : StateMachineBehaviour {

    public GameObject shamanPrefab;
    public Sprite spiritShaman;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if(animator.gameObject.GetComponent<Aztec>()!=null && animator.gameObject.GetComponent<Aztec>().ranged)
        {
            Debug.Log("Bastard is out");
            GameObject instance = Instantiate(shamanPrefab);
            instance.GetComponent<Animator>().enabled = false;
            instance.GetComponent<SpriteRenderer>().sprite = spiritShaman;
            instance.GetComponent<Aztec>().moveDirection = new Vector2(1,0);
            instance.GetComponent<Aztec>().legit = false;
            instance.transform.position = animator.transform.position;
            Destroy(instance.transform.GetChild(0).gameObject);
        }
        
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
    
	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {   
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
