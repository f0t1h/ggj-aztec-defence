﻿using UnityEngine;
using Game;
using System.Collections;

public class Aztec : ITrooper 
{
    public bool legit;
	public static IEnumerator Spawn( int count )
    {
        for(int i = 0; i < count; i++)
        {
            //GameManager._singleton.azCount++;
			
            /*if (GameManager._singleton.AReserves.childCount > 0)
            {
                
                GameObject spawn = GameManager._singleton.AReserves.GetChild(0).gameObject;
               
                spawn.GetComponent<Aztec>().enabled = true;
                spawn.GetComponent<SpriteRenderer>().enabled = true;
                spawn.GetComponent<Animator>().enabled = true;
                spawn.GetComponent<Aztec>().hitPoints = spawn.GetComponent<Aztec>().maxHitPoints;
                spawn.transform.tag = "AztecWarrior";

                spawn.transform.position = Constants.AZTEC_SPAWN_LOC;
                spawn.transform.SetParent(null);
            }
            else
            {
                Instantiate(GameManager._singleton.AztecPrefab);
            }*/
			
			Aztec azt = GameManager._singleton.aztecs.Pop();
            azt.legit = true;
			azt.transform.position = Constants.AZTEC_SPAWN_LOC;
            
            yield return new WaitForSeconds(1F);
        }
        

    }
	
    protected override void move()
    {
        // Checks Here
		if( !isDead )
		{
			ITrooper enemy = EnemyInFront();
			if (enemy == null)
			{
				_animator.SetBool("Walking", true);
				_animator.SetBool("Attacking", false);
				//Vector3 _pos = new Vector3(transform.position.x -1 , transform.position.y, 0);
				//transform.position = _pos;
				/*if( ranged && ( transform.position.x >= GameManager._singleton.shamanLimitX ) )
				{
					_animator.SetBool("Walking", false);
					_animator.SetBool("Attacking", false);
				}
				else*/
					transform.Translate(moveDirection * Time.deltaTime);
				
			}
			else if(_canAttack)
			{
				_animator.SetBool("Attacking", true);
				_animator.SetBool("Walking", false);
				if (ranged)
				{
					GameObject _projectile = Instantiate(projectile);
					_projectile.GetComponent<ProjectileMove>().owner = gameObject;
				}
				else
				{
					enemy.Damage(_attackDamage);
				}
				GetComponent<AudioSource>().Play();
				Invoke("StopAudio", 1F);
				
				_canAttack = false;

                Invoke("CoolDown", 2 );
            }
		}
    }
	
    public override void Damage( float val)
    {//print(name + " " + hitPoints);
        hitPoints -= val;
		
        if( !isDead && hitPoints <= 0 )
        {
			/*GetComponent<Aztec>().enabled = false;
			GetComponent<SpriteRenderer>().enabled = false;
			gameObject.GetComponent<Animator>().enabled = false;
			transform.tag = "DeadAztec";*/
			_animator.SetBool( "Dead", true );
			Invoke( "Die", 1f );
			
			isDead = true;
		}
    }
	
	void Die()
	{
        if (legit)
        {
            GameManager._singleton.aztecs.Push(this);
            GameManager._singleton.azCount--;
        }
		
	}
}
