﻿using UnityEngine;

public class CameraAlign : MonoBehaviour 
{
	public SpriteRenderer alignTo;
	public Transform eastBorder;
	
	void Awake()
	{
		Camera cam = Camera.main;
		
		Vector3 rendererExtents = alignTo.bounds.extents;
		float leftPos = alignTo.transform.position.x - rendererExtents.x;
		cam.orthographicSize = rendererExtents.y;
		float cameraWidth = cam.orthographicSize * cam.aspect;
		
		Vector3 v = cam.transform.position;
		v.x = leftPos + cameraWidth;
		cam.transform.position = v;
		
		Vector3 v2 = eastBorder.position;
		v2.x = v.x + cameraWidth + 1f;
		eastBorder.position = v2;
	}
}
