﻿using UnityEngine;

public class CameraSwitch : MonoBehaviour 
{
	public static CameraSwitch instance = null;
	public GameObject mainCam;
	public GameObject ragdollCam;
	public Sacrifice sacrifice;
	
	void Awake()
	{
		instance = this;
	}
	
	public void ReturnToMainCam()
	{
		ragdollCam.SetActive( false );
		//mainCam.SetActive( true );
	}
	
	public void RagdollCam( int amount )
	{
		sacrifice.amount = amount;
		//mainCam.SetActive( false );
		ragdollCam.SetActive( true );
	}
}
