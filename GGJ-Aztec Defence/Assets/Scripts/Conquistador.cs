﻿using UnityEngine;
using Game;
using System.Collections;

public class Conquistador : ITrooper 
{
	public GameObject particlePrefab;
	public Transform particlePoint;
	
    public static GameObject Spawn()
    {
        int val = Random.Range(0, 3);
		
        /*if(GameManager._singleton.SReserves.childCount > 0)
        {
            GameObject spawn = GameManager._singleton.SReserves.GetChild(0).gameObject;
            spawn.GetComponent<Conquistador>().enabled = true;
            spawn.GetComponent<SpriteRenderer>().enabled = true;
            spawn.GetComponent<Conquistador>().hitPoints = spawn.GetComponent<Conquistador>().maxHitPoints;
            spawn.transform.position = Constants.SPANISH_SPAWN_LOC-Vector3.up*val*Time.deltaTime;
            spawn.GetComponent<Conquistador>().range = spawn.GetComponent<Conquistador>().range + val;
            spawn.transform.SetParent(null);
        }
        else
        {
            Instantiate(GameManager._singleton.ConqPrefab);
        }
        return null;*/  
		
		Conquistador conq = GameManager._singleton.conquistadors.Pop();
		conq.transform.position = Constants.SPANISH_SPAWN_LOC-Vector3.up*val*0.3f;
        
        return conq.gameObject;
    }
	
	public static IEnumerator Spawn( int count )
	{
		for(int i = 0; i < count; i++)
        {
            int val = Random.Range(0, 3);
			Conquistador conq = GameManager._singleton.conquistadors.Pop();
			conq.transform.position = Constants.SPANISH_SPAWN_LOC-Vector3.up*val*Time.deltaTime;
            conq._attackDamage = GameManager._singleton.ConqMeleePrefab.GetComponent<Conquistador>()._attackDamage + GameManager._singleton.gameStage * 2;
            conq.maxHitPoints = GameManager._singleton.ConqMeleePrefab.GetComponent<Conquistador>().maxHitPoints + GameManager._singleton.gameStage * 5;
            conq.hitPoints = conq.maxHitPoints;
            yield return new WaitForSeconds(1F);
        }
	}
   
    

    protected override void move()
    {
        // Checks Here
		if( !isDead )
		{
			ITrooper enemy = EnemyInFront();
			if (enemy == null)
			{
				if( IsTowerInRange() )
				{
					if(_canAttack)
					{
						if (ranged)
						{
							GetComponent<AudioSource>().Play();
							Invoke("StopAudio", 1F);
							
							if( particlePrefab != null )
							{
								GameObject go = Instantiate( particlePrefab, particlePoint.position, Quaternion.identity ) as GameObject;
								Destroy( go, 0.8f );
							}
						}
						
						Tower.instance.Damage(_attackDamage);
						_animator.SetBool("Attacking", true);
						_animator.SetBool("Walking", false);
						
						_canAttack = false;
						Invoke("CoolDown", 2);

					}
				}
				else
				{
					_animator.SetBool("Walking", true);
					_animator.SetBool("Attacking", false);
					//Vector3 _pos = new Vector3(transform.position.x -1 , transform.position.y, 0);
					//transform.position = _pos;
					transform.Translate(moveDirection * Time.deltaTime);
				}
			}
			/*else if(_canAttack &&  Mathf.Abs(transform.position.x - enemy.transform.position.x) < enemy.GetComponent<Aztec>().range)
			{
				transform.Translate(-moveDirection * Time.deltaTime);

			}*/
			else if(_canAttack)
			{
				if (ranged)
				{
					GameObject _projectile = Instantiate(projectile);
					_projectile.GetComponent<ProjectileMove>().owner = gameObject;
					GetComponent<AudioSource>().Play();
					Invoke("StopAudio", 1F);
					
					if( particlePrefab != null )
					{
						GameObject go = Instantiate( particlePrefab, particlePoint.position, Quaternion.identity ) as GameObject;
						Destroy( go, 0.8f );
					}
				}
				else
				{
					enemy.Damage(_attackDamage);
				}
				_animator.SetBool("Attacking", true);
				_animator.SetBool("Walking", false);
				
				_canAttack = false;
				Invoke("CoolDown", 50 * Time.deltaTime);

			}
		}
    }
	
	public override void Damage(float val)
	{
		hitPoints -= val;
		if( !isDead && hitPoints <= 0)
		{
			/*gameObject.GetComponent<Conquistador>().enabled = false;
			gameObject.GetComponent<SpriteRenderer>().enabled = false;
			gameObject.transform.position = Constants.RESERVES_LOC;*/
			
			
			_animator.SetBool( "Dead", true );
			Invoke( "Die", 1f );
			
			isDead = true;
		}
    }
	
	void Die()
	{
		GameManager._singleton.conquistadors.Push( this );
		GameManager._singleton.conCount--;
	}
}
