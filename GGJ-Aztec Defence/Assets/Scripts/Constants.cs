﻿using UnityEngine;
namespace Game
{

    public static class Constants {
		public static float TOWER_X = -10F;
        public static Vector3 RESERVES_LOC = new Vector3(-1000, -1000, -1000);
        public static Vector3 SPANISH_SPAWN_LOC = new Vector3(7.42F, -1F, 0);
        public static Vector3 AZTEC_SPAWN_LOC = new Vector3(-5F, -1F ,0);

    }
}

