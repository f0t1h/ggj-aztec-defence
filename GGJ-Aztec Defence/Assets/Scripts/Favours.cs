﻿using UnityEngine;
using System.Collections.Generic;
using Game;
using UnityEngine.UI;
public class Favours : MonoBehaviour {

    // Use this for initialization
    public GameObject war_prefab;
    public GameObject smite;
    public int[] Costs;
    int favors;
    public  AudioSource source;
    public Sprite[] sprites;
    public Image shouter;
    public GameObject projectile;
    public RuntimeAnimatorController[] C_anims;
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        favors = GameManager._singleton.godFavor;
    }
    
    public void IncreaseGrowthRate(float growth)
    {
        
        if (costCheck(Costs[0]))
        {

            GameManager._singleton.growth += growth;
            GameManager._singleton.godFavor -= Costs[0];
            GameManager._singleton.favourText.text = "Favor: " + GameManager._singleton.godFavor;
            source.Play();
            shouter.sprite = sprites[0];
            shouter.enabled = true;
            Invoke("revertImage", 2F);
        }
    }

    public void IncreaseDamage(float amount)
    {
        if (costCheck(Costs[3]))
        {

            GameManager._singleton.dpsMultiplier += amount;
            GameManager._singleton.godFavor -= Costs[3];
            GameManager._singleton.favourText.text = "Favor: " + GameManager._singleton.godFavor;
            source.Play();
            shouter.sprite = sprites[3];
            shouter.enabled = true;
            Invoke("revertImage", 2F);
        }
       

    }

    public void RaiseDead()
    {
        if (costCheck(Costs[4])){
            GameManager._singleton.godFavor -= Costs[4];
            GameManager._singleton.favourText.text = "Favor: " + GameManager._singleton.godFavor;
            source.Play();
            shouter.sprite = sprites[4];
            shouter.enabled = true;
            Invoke("revertImage", 2F);
        }
    }
    public void Smite()
    {
        if (costCheck(Costs[2]) ){
            int _x2 = (int)GameManager._singleton.eastBorder.position.x;
            int _x1 = (int)GameManager._singleton.westBorder.position.x;
            int _y = (int)GameManager._singleton.westBorder.position.y;

            GameObject tmp = Instantiate(smite);
            tmp.transform.position = new Vector3((_x1 + _x2) / 2, _y + 3, 0);
            GameManager._singleton.godFavor -= Costs[2];
            GameManager._singleton.favourText.text = "Favor: " + GameManager._singleton.godFavor;
            shouter.sprite = sprites[2];
            shouter.enabled = true;
            Invoke("revertImage", 2F);
        }
        
        
        
    }

    public void MirrorImage()
    {
        if (costCheck(Costs[1]))
        {
            GameObject tmp = Instantiate(war_prefab);
            Aztec conq = tmp.GetComponent<Aztec>();
            conq.transform.position = Constants.AZTEC_SPAWN_LOC;
            int rand = Random.Range(0, 2);
            conq.gameObject.GetComponent<Animator>().runtimeAnimatorController = C_anims[rand];
            conq.gameObject.GetComponent<Animator>().applyRootMotion = true;
            if (rand == 0)
            {
                conq._attackDamage = 25;
                conq.range = 5;
                conq.moveDirection *= 2;
                conq.transform.localScale = new Vector3(0.12F, 0.12F, 1);
                conq.ranged = true;
                conq.projectile = projectile;
                conq.projectile.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);

            }
            else
            {
                conq.range = 1;
                conq.transform.localScale = new Vector3(0.17F, 0.17F, 1);
                conq.GetComponent<SpriteRenderer>().flipX = true;
            }
            conq.GetComponent<SpriteRenderer>().color = new Color(137F/255, 167F / 255, 255F / 255);
            conq.gameObject.layer = LayerMask.NameToLayer("Aztec");
            conq.gameObject.tag = "AztecWarrior";
            Destroy(conq.transform.GetChild(0).gameObject);
            
            Destroy(conq.gameObject, 15F);
            GameManager._singleton.godFavor -= Costs[1];
            GameManager._singleton.favourText.text = "Favor: " + GameManager._singleton.godFavor;
            source.Play();
            shouter.sprite = sprites[1];
            shouter.enabled = true;
            Invoke("revertImage", 2F);
        }
    }

    public bool costCheck(int i)
    {
        return favors >= i;
    }

    public void revertImage()
    {
        shouter.enabled = false;
    }

}
