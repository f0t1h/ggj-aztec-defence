﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FireDPS : MonoBehaviour {

    // Use this for initialization
    float amount;
    public float limit;
    public float dps;
	private int multiplier = 1;
	private bool init = false;
	
	private AudioSource audioS;

	void Start () {
        amount = 0;
        audioS = GetComponent<AudioSource>();
    }
	
	void Update () {
        amount+=Time.deltaTime;
        if( !init && amount >= limit)
        {
			multiplier = -1;
            Destroy(gameObject, 0.5f);
			init = true;
        }
		
		audioS.volume += multiplier * Time.deltaTime * 3f;
	}



    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<Conquistador>() != null )
            other.gameObject.GetComponent<Conquistador>().Damage(dps*Time.deltaTime);
    }
}
