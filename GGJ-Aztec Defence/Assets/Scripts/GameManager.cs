﻿using UnityEngine;
using System.Collections;
using Game;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    [SerializeField]
    public int population;
    [SerializeField]
    public float growth;
    [SerializeField]
    public int gameStage;
    //[SerializeField]
   // GameObject c_text;
    //[SerializeField]
    //GameObject s_text;
    public int conCount;
    public int azCount;
    public static GameManager _singleton;
    [SerializeField]
    public GameObject ConqMeleePrefab;
    [SerializeField]
    public GameObject ConqRangedPrefab;
    [SerializeField]
    public GameObject AztecMeleePrefab;
    [SerializeField]
    public GameObject AztecRangedPrefab;
    [SerializeField]
    public GameObject levelEnd;
    public bool stop;
    public int godFavor;
    public float dpsMultiplier;
    [SerializeField]
    public Transform shamanLimit;
	public float shamanLimitX;
	
	public PoolScript<Conquistador> conquistadors;
	public PoolScript<Aztec> aztecs;

    public Transform eastBorder;
    public Transform westBorder;
	
	private AudioSource audio;
    public Text populationText;
    public Text favourText;
    // Use this for initialization

    GameObject endlvlmngr;
	
	void Awake()
	{
		_singleton = this;
	}


    void Start () {
        conCount = 1;
        azCount = 1;
        // stop = false;
		
		audio = GetComponent<AudioSource>();
		
		conquistadors = new PoolScript<Conquistador>();
		aztecs = new PoolScript<Aztec>();
		
		conquistadors.Populate( ConqMeleePrefab.GetComponent<Conquistador>(), ConqRangedPrefab.GetComponent<Conquistador>(), 5 );
		aztecs.Populate( AztecMeleePrefab.GetComponent<Aztec>(), AztecRangedPrefab.GetComponent<Aztec>(), 5 );
		
		Constants.AZTEC_SPAWN_LOC = westBorder.position;
		Constants.SPANISH_SPAWN_LOC = eastBorder.position;
		
		shamanLimitX = shamanLimit.position.x;

        favourText.text = "Favor: " + godFavor;
        populationText.text = "Pop: " + population;

        StartCoroutine( Conquistador.Spawn(1) );
		StartCoroutine( Aztec.Spawn(1) );
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P))
        {
            gameObject.GetComponent<Favours>().Smite();
            favourText.text = "Favor: " + godFavor;
        }
		
        if(conCount <= 0 && !stop)
        {
            gameStage++;
            population += (int)(population * growth/100);
            StopAllCoroutines();
			
			GameObject[] aztcs = GameObject.FindGameObjectsWithTag( "AztecWarrior" );
			for( int i = 0; i < aztcs.Length; i++ )
			{
                if (aztcs[i].GetComponent<Aztec>().legit)
                {
                    aztecs.Push(aztcs[i].GetComponent<Aztec>());
                    population += 5;
                }
                else
                {
                    Destroy(aztcs[i].gameObject);
                }
			}
			
			audio.Play();
			
            /*while (azCount > 0)
            {
                Debug.Log(GameObject.FindGameObjectWithTag("AztecWarrior"));
                if (GameObject.FindGameObjectWithTag("AztecWarrior") != null)
                {
                    GameObject.FindGameObjectWithTag("AztecWarrior").GetComponent<Aztec>().Damage(9000);
                    population += 6;

                }
            }*/
            endlvlmngr =  Instantiate(levelEnd);
            
            Transform sm = endlvlmngr.transform.Find("Slider Manager");
            sm.GetComponent<SliderManager>().idle.maxValue = population;
            sm.GetComponent<SliderManager>().army.maxValue = population;
            sm.GetComponent<SliderManager>().sacrifice.maxValue = population;
            stop = true;
            

        }
        //s_text.GetComponent<Text>().text = gameStage + "";
        //c_text.GetComponent<Text>().text = conCount + "";
	}

    public void startLevel()
    {
        Transform sm = endlvlmngr.transform.Find("Slider Manager");
        
        
        int _toidle = (int)sm.GetComponent<SliderManager>().idle.value;
        int _toarmy = (int)sm.GetComponent<SliderManager>().army.value/5;
        int _tosacrifice = (int)sm.GetComponent<SliderManager>().sacrifice.value;
        stop = false;
        population = _toidle;
        azCount = _toarmy;
        conCount = (int)gameStage +1;
        godFavor += _tosacrifice;
        Destroy(endlvlmngr);
        favourText.text = "Favor: " + godFavor;
        populationText.text = "Pop: " + population;
        StartCoroutine(Conquistador.Spawn(conCount)); //conCount = 0;
        
        StartCoroutine(Aztec.Spawn(azCount));//azCount = 0;
            

        
    }


}
