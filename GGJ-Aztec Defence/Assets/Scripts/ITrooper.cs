﻿using UnityEngine;
using Game;

public abstract class ITrooper : MonoBehaviour 
{
	protected  Animator _animator;
	protected  Transform _transform;
	
	[SerializeField]
	public float range;
	[SerializeField]
	public bool ranged;
	
	[SerializeField]
	public float  _attackDamage;
	protected bool _canAttack;
	
	public GameObject projectile;

	[SerializeField]
	public float maxHitPoints;
	public float hitPoints;
	
	[SerializeField]
	public Vector2 moveDirection;
	
	[SerializeField]
	public LayerMask enemyLayerMask;
	
	protected bool isDead = false;

	void Start()
	{
		_animator = gameObject.GetComponent<Animator>();
		_transform = transform;
	}
	
	void OnEnable()
	{
		hitPoints = maxHitPoints;
		_canAttack = true;
		isDead = false;
	}

	void Update() 
	{
		move();
	}
	
	public abstract void Damage( float val );
	protected abstract void move();
	
	protected ITrooper EnemyInFront()
	{
		RaycastHit2D hit = Physics2D.Raycast( _transform.position, moveDirection, range, enemyLayerMask );

		if( hit.collider != null )
			return hit.collider.GetComponent<ITrooper>();

		return null;
	}
	
	protected bool IsTowerInRange()
	{
		return Mathf.Abs( _transform.position.x - Constants.TOWER_X ) < range;
	}

	public void CoolDown()
	{
		_canAttack = true;
	}
	
	protected void StopAudio()
	{
		GetComponent<AudioSource>().Stop();
	}
}
