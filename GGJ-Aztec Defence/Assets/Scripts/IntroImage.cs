﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class IntroImage : MonoBehaviour
{
	public Image img;
	private float startTime;
	
	void Awake()
	{
		Time.timeScale = 0f;
		startTime = Time.realtimeSinceStartup + 1f;
	}
	
	void Start()
	{
		GameManager._singleton.stop = true;
	}
	
	void Update() 
	{
		if( Time.realtimeSinceStartup > startTime )
		{
			if( img.fillAmount >= 1f )
				EventSystem.current.GetComponent<StandaloneInputModule>().enabled = true;
			else
				img.fillAmount += Time.unscaledDeltaTime * 2f;
		}
			
		if( Time.timeScale == 1f )
		{
			Color c = img.color;
			c.a -= 3f * Time.deltaTime;
			img.color = c;
		}
	}
	
	public void OnClick()
	{
		Time.timeScale = 1f;
		Invoke( "asdasd", 0.3f );
		Destroy( gameObject, 0.5f );
		
	}
	
	void asdasd()
	{
		GameManager._singleton.stop = false;
	}
}
