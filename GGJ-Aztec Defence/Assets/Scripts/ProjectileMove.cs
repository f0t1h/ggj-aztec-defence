﻿using UnityEngine;
using System.Collections;

public class ProjectileMove : MonoBehaviour {

    Vector3 direction;
    public GameObject owner;
    bool isAztec;
	// Use this for initialization
	void Start () {
        transform.position = owner.transform.position;
		Destroy( gameObject, 2f );
        direction = owner.GetComponent<ITrooper>().moveDirection;

        isAztec = owner.GetComponent<Aztec>() != null;
    }
	
	// Update is called once per frame
	void Update () {
        transform.Translate(direction*Time.deltaTime*15);
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (isAztec)
        {
            if (other.gameObject.GetComponent<Conquistador>() != null)
            {
                other.gameObject.GetComponent<Conquistador>().Damage(owner.GetComponent<Aztec>()._attackDamage);
                Destroy(gameObject);
            }
            Debug.Log("ASD");
        }
        else
        {
            if (other.gameObject.GetComponent<Aztec>() != null)
            {
                other.gameObject.GetComponent<Aztec>().Damage(owner.GetComponent<Conquistador>()._attackDamage);
                Destroy(gameObject);
            }
            Debug.Log("WASD");
        }
        Debug.Log("ASDasdasd");

    }
}
