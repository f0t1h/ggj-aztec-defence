﻿using UnityEngine;
using System.Collections;

public class Sacrifice : MonoBehaviour {

    // Use this for initialization
    public int amount;
    public GameObject prefab;
	private GameObject[] objs;
	
	void OnEnable () {
		StopAllCoroutines();
		StartCoroutine( Spawn() );
		
           
	}
	
	IEnumerator Spawn()
	{
		yield return null;
		yield return null;
		
		objs = new GameObject[amount];
	    for(int i = 0;i< amount; i++)
        {
            GameObject tmp = Instantiate(prefab);
            tmp.transform.rotation = Random.rotation;
           
            int _x = (int)Random.Range(-4, 4);

            int _y = (int)Random.Range(10, 75);
            
            int _z = (int)Random.Range(0, 25);
            tmp.transform.position = new Vector3(_x, _y, _z);
			objs[i] = tmp;
		}
	}
	
	void OnDisable()
	{
		for( int i = objs.Length - 1; i >= 0; i-- )
			Destroy( objs[i] );
	}
}
