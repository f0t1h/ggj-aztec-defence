﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SliderManager : MonoBehaviour {


    [SerializeField]
    public Slider sacrifice;
    [SerializeField]
    public Slider army;
    [SerializeField]
    public Slider idle;
    int s_val, a_val, i_val;
    int peopleCount;

	public CanvasGroup alph;
	
    [SerializeField]
    Text s_num;

    [SerializeField]
    Text a_num;

    [SerializeField]
    Text i_num;

    [SerializeField]
    Text pop;

    [SerializeField]
    Text year;
    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        peopleCount = GameManager._singleton.population;
        int _sum = (int)(sacrifice.value + army.value + idle.value);
        int _dif = (int)(_sum - peopleCount);
        pop.text = "There are " + peopleCount + " Aztecs in the village..";
        year.text = "Year " + (GameManager._singleton.gameStage +1518);
        if (army.value % 5 != 0)
        {
            army.value = Mathf.Floor(army.value/5)*5;
        }
        if (_sum > peopleCount)
        {
            if (s_val != sacrifice.value)
            {
                army.value = army.value - _dif / 2;
                idle.value = idle.value - _dif / 2- (_dif % 2 == 0 ? 0 : 1);

            }
            else if (a_val != army.value)
            {

                sacrifice.value = sacrifice.value - _dif / 2- (_dif % 2 == 0 ? 0 : 1);
                idle.value = idle.value - _dif / 2;
            }
            else if (i_val != idle.value)
            {

                army.value = army.value - _dif / 2;
                sacrifice.value = sacrifice.value - _dif / 2-(_dif % 2 == 0 ? 0 : 1);
            }
            else
            {
                idle.value = idle.value - _dif / 3;
                army.value = army.value - _dif / 3;
                sacrifice.value = sacrifice.value - _dif / 3;
                float max = Mathf.Max(idle.value, army.value, sacrifice.value);
                if(idle.value != max && idle.value>0)
                {
                    idle.value -= _dif % 3;
                }if (sacrifice.value != max && sacrifice.value > 0)
                {
                    sacrifice.value -= _dif % 3;
                }
                if (army.value != max && army.value > 0)
                {
                    army.value -= _dif % 3;
                }
            }
        }
        s_val = (int)sacrifice.value;
        a_val = (int)army.value;
        i_val = (int)idle.value;
        s_num.text = s_val +"";
        a_num.text = a_val + "";
        i_num.text = i_val + "";
    }

    public void SliderChanged()
    { 
        
        

    }
	
	IEnumerator startLevelCoroutine()
	{
		alph.alpha = 0f;
		
		if((int) sacrifice.value!=0){
		
			CameraSwitch.instance.RagdollCam( (int) sacrifice.value );
			yield return new WaitForSeconds( 5f );
			CameraSwitch.instance.ReturnToMainCam();
		}
		peopleCount = GameManager._singleton.population;
        int _sum = (int)(sacrifice.value + army.value + idle.value);
        if (_sum < peopleCount)
            idle.value += (peopleCount - _sum);
			
        GameManager._singleton.startLevel();
	}
    
    public void startLevel()
    {
		StartCoroutine( startLevelCoroutine() );
    }
}
