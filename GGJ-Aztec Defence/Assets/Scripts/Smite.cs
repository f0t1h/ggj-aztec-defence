﻿using UnityEngine;
using System.Collections;

public class Smite : MonoBehaviour {

    // Use this for initialization
    public GameObject s_prefab;
    public GameObject soundPre;
	void Start () {
        GameObject voc = Instantiate(soundPre);
        Destroy(voc, 4F);
        for (int i = 0; i < 5; i++)
        {
            GameObject tmp = Instantiate(s_prefab);
            tmp.transform.position = transform.position + 3.4F * Vector3.down + (i-2)*Vector3.right;
            
        }
        Destroy(gameObject, 0.2f);
    }
}
