﻿using UnityEngine;
using UnityEngine.UI;
using Game;

public class Tower : MonoBehaviour
{
	public static Tower instance;
	private float hitPoints;
	public float maxHitPoints = 200;
	
	public Transform rightPos;
	public GameObject projectilePrefab;
	public Slider slider;
	public Slider healthSlider;
	private CanvasGroup sliderAlpha;
	private CanvasGroup healthSliderAlpha;
	
	private Transform tr;
	
	public float cooldown = 1f;
	private float nextTime = 0f;
	
	private float gravity;
	private Camera cam;
	
	public float preferredAngle = 30f;
	
	public float projectileSpeed = 5f;
	public float timeMultiplier = 0.2f;
	
	void Awake() { instance = this; }
	
	void Start()
	{
		tr = transform;
		sliderAlpha = slider.GetComponent<CanvasGroup>();
		healthSliderAlpha = healthSlider.GetComponent<CanvasGroup>();
		cam = Camera.main;
		gravity = Mathf.Abs( Physics2D.gravity.y );
		
		hitPoints = maxHitPoints;
		healthSlider.maxValue = hitPoints;
		healthSlider.value = hitPoints;
	}
	
	void Update()
	{
        //print(Input.mousePosition.y);
		if( Input.GetMouseButtonDown(0) && !GameManager._singleton.stop && Input.mousePosition.y < Screen.height * 0.8f)
		{
			if( Time.time >= nextTime )
			{
				Vector2 direction = cam.ScreenToWorldPoint( Input.mousePosition ) - tr.position;
				
				if( direction.x > 0 )
				{
					Rigidbody2D rb = ( (GameObject) Instantiate( projectilePrefab, tr.position, Quaternion.identity ) ).GetComponent<Rigidbody2D>();
					float t = Mathf.Sqrt( direction.x ) * timeMultiplier;
					
					float vx = direction.x / t;
					float vy = ( gravity * t * t + 2 * direction.y ) / ( 2 * t );
					rb.velocity = new Vector2( vx, vy );
					
					nextTime = Time.time + cooldown;
				}
			}
		}
		
		float c = sliderAlpha.alpha;
		if( Time.time >= nextTime )
		{
			c -= 2f * Time.deltaTime;
			slider.value = 1f;
		}
		else
		{
			c += 2f * Time.deltaTime;
			slider.value = 1f - ( nextTime - Time.time ) / cooldown;
		}
		c = Mathf.Clamp01( c );
		sliderAlpha.alpha = c;
		
		if( hitPoints < maxHitPoints )
		{
			c = healthSliderAlpha.alpha;
			c += 2f * Time.deltaTime;
			c = Mathf.Clamp01( c );
			healthSliderAlpha.alpha = c;
		}
	}
	
	public void Damage( float amount )
	{
		hitPoints -= amount;
		healthSlider.value = hitPoints;

		if( hitPoints <= 0 )
		{
			Application.LoadLevel(1);
		}
	}
}
