﻿using UnityEngine;

public class TowerProjectile : MonoBehaviour 
{
	private Transform tr;
    private float ground;
    [SerializeField]
    GameObject firePrefab;
	void Start()
	{
		tr = transform;
		//Destroy( gameObject, 10f*Time.deltaTime );
        ground = GameManager._singleton.shamanLimit.transform.position.y;
    }
	
	void LateUpdate()
	{
		if( tr.position.y <= ground )
			Explode();
	}
	
	void Explode()
	{
        int _amount = Random.Range(2, 4);
        for (int i = 0; i < _amount; i++)
        {
            GameObject tmp = Instantiate(firePrefab);
            tmp.transform.position = transform.position - i * Vector3.left + Vector3.up * Random.value * 0.3f;
			tmp.GetComponent<Animator>().Play( "FireAnim", -1, Random.value );
		}
		
		Transform child = tr.GetChild(0);
		child.parent = null;
		child.GetComponent<TrailRenderer>().time = 0.25f;
        Destroy( gameObject );
	}
}
